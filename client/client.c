#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include<ctype.h>
#include<time.h>
#include<unistd.h>
#include<dirent.h>
#include<sys/stat.h>
#include<sys/types.h>
#include<sys/socket.h>
#include<arpa/inet.h>
#include<netinet/in.h>
#define PORT 4443

struct permitted
{
	char name[1005],password[1005];
};

void tulisLog(char *cmd,char *nama)
{
	time_t rawtime;
	struct tm *timeinfo;
	time(&rawtime);
	timeinfo=localtime(&rawtime);
	char ingfoWaktu[1000],path[1005];
	FILE *file=fopen(path,"ab");
	snprintf(path, sizeof path,"../database/log/log%s.log",nama);
	sprintf(ingfoWaktu, "%d-%.2d-%.2d %.2d:%.2d:%.2d:%s:%s;\n",timeinfo->tm_year + 1900, timeinfo->tm_mon + 1, timeinfo->tm_mday, timeinfo->tm_hour, timeinfo->tm_min, timeinfo->tm_sec, nama, cmd);
	fputs(ingfoWaktu, file);
	fclose(file);
	return;
}

int cekpermitted(char *username,char *password)
{
	FILE *fp=fopen("../database/databases/user.dat","rb");
	struct permitted user;
	int id,found=0;
	while(1)
    {
		fread(&user,sizeof(user),1,fp);
		if(strcmp(user.name,username)==0&&strcmp(user.password, password)==0)found=1;
		if(feof(fp)) break;
	}
	fclose(fp);
	if(!found)
    {
		printf("Tidak diizinkan\n");
		return 0;
	}
	else return 1;
}

int main(int argc,char *argv[])
{
	int permitted=0,id_user=geteuid(),clientSocket, ret;
	char database_used[1000],buffer[32000];
	if(geteuid() == 0) permitted=1;
	else permitted = cekpermitted(argv[2],argv[4]);
	if(permitted==0) return 0;
	struct sockaddr_in alamatServer;
	clientSocket = socket(AF_INET, SOCK_STREAM, 0);
	if(clientSocket<0) printf("[-]Terdapat eror di koneksi.\n"),exit(1);
	printf("[+]Socket Klien berhasil dibuat.\n");
	memset(&alamatServer, '\0', sizeof(alamatServer));
	alamatServer.sin_family = AF_INET;
	alamatServer.sin_port = htons(PORT);
	alamatServer.sin_addr.s_addr = inet_addr("127.0.0.1");
	ret = connect(clientSocket, (struct sockaddr*)&alamatServer, sizeof(alamatServer));
	if(ret < 0) printf("[-]Terdapat eror di koneksi.\n"),exit(1);
	printf("[+]Terkoneksi ke Server.\n");
	while(1)
	{
		printf("Client: \t");
		char input[1005],copyinput[1005],cmd[100][1005],*token;
		int i=0,salahCMD=0;
		scanf(" %[^\n]s", input);
		strcpy(copyinput, input);
		token=strtok(input, " ");
		printf("Token: %s\n",token);
		while(token!=NULL) strcpy(cmd[i++],token),token=strtok(NULL," ");
		if(strcmp(cmd[0], "CREATE")==0)
		{
			if(strcmp(cmd[1],"USER")==0&&strcmp(cmd[3],"IDENTIFIED")==0&&strcmp(cmd[4],"BY")==0)
				snprintf(buffer, sizeof buffer,"cUser:%s:%s:%d",cmd[2],cmd[5],id_user),
				send(clientSocket, buffer,strlen(buffer),0);
			else if(strcmp(cmd[1],"DATABASE")==0)
				snprintf(buffer,sizeof buffer,"cDatabase:%s:%s:%d",cmd[2], argv[2],id_user),
				send(clientSocket, buffer, strlen(buffer),0);
			else if(strcmp(cmd[1], "TABLE")==0)
				snprintf(buffer, sizeof buffer, "cTable:%s", copyinput),
				send(clientSocket, buffer, strlen(buffer),0);
		}
		else if(strcmp(cmd[0], "GRANT")==0 && strcmp(cmd[1], "PERMISSION")==0&&strcmp(cmd[3], "INTO")==0)
			snprintf(buffer, sizeof buffer, "gPermission:%s:%s:%d", cmd[2],cmd[4], id_user),
			send(clientSocket, buffer, strlen(buffer), 0);
		else if(strcmp(cmd[0], "USE")==0)
			snprintf(buffer, sizeof buffer, "uDatabase:%s:%s:%d", cmd[1], argv[2], id_user),
			send(clientSocket, buffer, strlen(buffer), 0);
		else if(strcmp(cmd[0], "cekCurrentDatabase")==0)
			snprintf(buffer, sizeof buffer, "%s", cmd[0]),
			send(clientSocket, buffer, strlen(buffer), 0);
		else if(strcmp(cmd[0], "DROP")==0)
		{
			if(strcmp(cmd[1], "DATABASE")==0)
				snprintf(buffer, sizeof buffer, "dDatabase:%s:%s", cmd[2], argv[2]),
				send(clientSocket, buffer, strlen(buffer), 0);
			else if(strcmp(cmd[1], "TABLE")==0)
				snprintf(buffer, sizeof buffer, "dtabel:%s:%s", cmd[2], argv[2]),
				send(clientSocket, buffer, strlen(buffer), 0);
			else if(strcmp(cmd[1], "COLUMN")==0)
				snprintf(buffer, sizeof buffer, "dColumn:%s:%s:%s", cmd[2], cmd[4] ,argv[2]),
				send(clientSocket, buffer, strlen(buffer), 0);
		}
        else if(strcmp(cmd[0], "INSERT")==0 && strcmp(cmd[1], "INTO")==0)
            snprintf(buffer, sizeof buffer, "insert:%s", copyinput),
			send(clientSocket, buffer, strlen(buffer), 0);
        else if(strcmp(cmd[0], "UPDATE")==0)
            snprintf(buffer, sizeof buffer, "update:%s", copyinput),
			send(clientSocket, buffer, strlen(buffer), 0);
        else if(strcmp(cmd[0], "DELETE")==0)
            snprintf(buffer, sizeof buffer, "delete:%s", copyinput),
            send(clientSocket, buffer, strlen(buffer), 0);
        else if(strcmp(cmd[0], "SELECT")==0)
            snprintf(buffer, sizeof buffer, "select:%s", copyinput),
            send(clientSocket, buffer, strlen(buffer), 0);
        else if(strcmp(cmd[0], ":exit")!=0)
			salahCMD=1,send(clientSocket,"Salah cmd",strlen("Salah cmd"),0);
		if(!salahCMD)
        {
			char namaSender[1005];
			if(id_user == 0) strcpy(namaSender, "root");
			else strcpy(namaSender, argv[2]);
			tulisLog(copyinput, namaSender);
		}
		if(strcmp(cmd[0],":exit")==0)
			send(clientSocket, cmd[0], strlen(cmd[0]), 0),
			close(clientSocket),printf("[-]Disconnected from server.\n"),exit(1);
		bzero(buffer,sizeof(buffer));
		if(recv(clientSocket, buffer, 1024, 0) < 0) printf("[-]Terdapat eror ketika menerima data.\n");
		else printf("Server: \t%s\n", buffer);
	}
	return 0;
}
