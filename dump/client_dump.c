#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include<ctype.h>
#include<time.h>
#include<unistd.h>
#include<dirent.h>
#include<sys/stat.h>
#include<sys/types.h>
#include<sys/socket.h>
#include<arpa/inet.h>
#include<netinet/in.h>
#define PORT 4443

struct permitted
{
	char name[10005],password[10005];
};

int cekStatus(char *username, char *password)
{
	FILE *fp=fopen("../database/databases/user.dat","rb");
	struct permitted user;
	int ketemu=0;
	while(1)
    {
		fread(&user,sizeof(user),1,fp);
		if(strcmp(user.name, username)==0&&strcmp(user.password, password)==0) ketemu=1;
		if(feof(fp)) break;
	}
	fclose(fp);
	if(!ketemu)
    {
		printf("Tidak diizinkan.\n");
		return 0;
	}
	else return 1;
}

int main()
{
	int permitted=0,id_user=geteuid(),ketemu=0;
	char database_used[1000],buffer[10005],cmd[100][10005];
	if(geteuid() == 0) permitted=1;
	else permitted = cekStatus(argv[2],argv[4]);
	if(permitted==0) return 0;
    char lokasi[10000];
	snprintf(lokasi, sizeof lokasi, "../database/log/log%s.log", argv[2]);
    FILE *fp = fopen(lokasi, "rb");
    while(fgets(buffer,10005,fp))
    {
        char *token,buff2[10005],*b=strstr(cmd[4], "USE");
        snprintf(buff2, sizeof buff2, "%s", buffer);
        token=strtok(buff2,":");
        int i=0,j=0;
        while(token!=NULL)strcpy(cmd[i++],token),token=strtok(NULL,":");
        if(b!=NULL)
        {
            char *tokens,cmdCopy[10005],cmdUse[100][10005];
            strcpy(cmdCopy,cmd[4]),tokens=strtok(cmdCopy,"; ");
            while(tokens!=NULL)strcpy(cmdUse[j++],tokens),tokens=strtok(NULL,"; ");
            if(!strcmp(cmdUse[1],argv[5]))ketemu=1;
            else ketemu=0;
        }
        if(ketemu)printf("%s",buffer);
    }
    fclose(fp);
}
