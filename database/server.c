#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include<ctype.h>
#include<time.h>
#include<unistd.h>
#include<dirent.h>
#include<sys/stat.h>
#include<sys/types.h>
#include<sys/socket.h>
#include<arpa/inet.h>
#include<netinet/in.h>
#define PORT 4443

struct db
{
	char database[1005],name[1005];
};

struct tabel
{
	char tipenya[100][1005],data[100][1005];
	int nKolom;
};

struct permitted
{
	char name[1005],pass[1005];
};

void bikinAkun(char *nama,char *pass)
{
	struct permitted user;
	strcpy(user.name,nama),strcpy(user.pass,pass);
	printf("%s %s\n", user.name,user.pass);
	char fname[]={"databases/user.dat"};
	FILE *fp=fopen(fname,"ab");
	fwrite(&user,sizeof(user),1,fp),fclose(fp);
}

int cekUser(char *username)
{
	FILE *fp=fopen("../database/databases/user.dat","rb");
	struct permitted user;
	int id,found=0;
	while(1)
    {
		fread(&user,sizeof(user),1,fp);
		if(strcmp(user.name, username)==0) return 1;
		if(feof(fp)) break;
	}
	fclose(fp);
	return 0;

}

void beriIzin(char *nama,char *database)
{
	struct db user;
	strcpy(user.name, nama),strcpy(user.database, database);
	printf("%s %s\n", user.name, user.database);
	FILE *fp=fopen("databases/permission.dat","ab");
	fwrite(&user,sizeof(user),1,fp),fclose(fp);
}

int cekAccDB(char *nama,char *database)
{
	FILE *fp=fopen("../database/databases/permission.dat","rb");
	struct db user;
	int id,found=0;
	printf("nama = %s  database = %s", nama, database);
	while(1)
    {
		fread(&user,sizeof(user),1,fp);
		if(strcmp(user.name,nama)==0&&strcmp(user.database,database)==0) return 1;
		if(feof(fp)) break;
	}
	fclose(fp);
	return 0;
}

int cariKolom(char *tabel,char *kolom)
{
	FILE *fp=fopen(tabel,"rb");
	struct tabel user;
	int id,found=0;
	fread(&user,sizeof(user),1,fp);
	for(int i=0;i<user.nKolom;i++)
		if(strcmp(user.data[i], kolom)==0) id=i;
    if(feof(fp))return -1;
	fclose(fp);
	return id;
}

int hapusKolom(char *tabel,int id)
{
	FILE *fp=fopen(tabel,"rb"),*fp1=fopen("sementara","wb");
	struct tabel user;
	int found=0;
	while(1)
    {
		fread(&user,sizeof(user),1,fp);
        if(feof(fp)) break;
		struct tabel user2;
		int iter=0;
		for(int i=0; i<user.nKolom,i!=id; i++)
			strcpy(user2.data[iter], user.data[i]),strcpy(user2.tipenya[iter], user.tipenya[i]),printf("%s\n", user2.data[iter++]);
		user2.nKolom = user.nKolom-1;
		fwrite(&user2,sizeof(user2),1,fp1);
	}
	fclose(fp),fclose(fp1),remove(tabel),rename("sementara", tabel);
	return 0;
}

int hapusTabel(char *tabel,char *namatabel)
{
    FILE *fp, *fp1;
	struct tabel user;
	int found=0;
	fp=fopen(tabel,"rb");
    fp1=fopen("sementara","ab");
	fread(&user,sizeof(user),1,fp);
    struct tabel user2;
	for(int i=0; i < user.nKolom; i++)
        strcpy(user2.data[i], user.data[i]),strcpy(user2.tipenya[i], user.tipenya[i]);
    user2.nKolom = user.nKolom,fwrite(&user2,sizeof(user2),1,fp1);
    fclose(fp),fclose(fp1),remove(tabel),rename("sementara", tabel);
	return 1;
}

int updateKolom(char *tabel,int id,char *ubah)
{
    FILE *fp=fopen(tabel,"rb"), *fp1=fopen("sementara","ab");
	struct tabel user;
	int found=0,dataX=0;
	while(1)
    {
		fread(&user,sizeof(user),1,fp);
        if(feof(fp)) break;
        struct tabel user2;
        int iter=0;
        for(int i=0; i< user.nKolom; i++)
        {
            if(i == id && dataX!=0) strcpy(user2.data[iter], ubah);
            else strcpy(user2.data[iter], user.data[i]);
            printf("%s\n", user2.data[iter]);
            strcpy(user2.tipenya[iter], user.tipenya[i]);
            printf("%s\n", user2.data[iter++]);
        }
        user2.nKolom=user.nKolom;
        fwrite(&user2,sizeof(user2),1,fp1),dataX++;
	}
	fclose(fp),fclose(fp1),remove(tabel),rename("sementara", tabel);
	return 0;
}

int updateKolomWhere(char *tabel,int id,char *ubah,int idUbah,char *where)
{
    FILE *fp=fopen(tabel,"rb"), *fp1=fopen("sementara","ab");
	struct tabel user;
	int found=0,dataX=0;
	while(1)
    {
		fread(&user,sizeof(user),1,fp);
        if(feof(fp)) break;
        struct tabel user2;
        int iter=0;
        for(int i=0; i< user.nKolom; i++)
        {
            if(i==id&&dataX!=0 && strcmp(user.data[idUbah], where)==0) strcpy(user2.data[iter], ubah);
            else strcpy(user2.data[iter], user.data[i]);
            printf("%s\n", user2.data[iter]);
            strcpy(user2.tipenya[iter],user.tipenya[i]);
            printf("%s\n", user2.data[iter++]);
        }
        user2.nKolom=user.nKolom,fwrite(&user2,sizeof(user2),1,fp1),dataX++;
	}
	fclose(fp),fclose(fp1),remove(tabel),rename("sementara", tabel);
	return 0;
}

int hapusTabelWhere(char *tabel,int id,char *kolom,char *where)
{
    FILE *fp=fopen(tabel,"rb"), *fp1=fopen("sementara","ab");
	struct tabel user;
	int found=0,dataX=0;
	while(1)
    {
        found = 0;
		fread(&user,sizeof(user),1,fp);
        if(feof(fp)) break;
        struct tabel user2;
        int iter=0;
        for(int i=0;i<user.nKolom;i++)
        {
            if(i==id&&dataX!=0&&strcmp(user.data[i],where)==0)found = 1;
            strcpy(user2.data[iter], user.data[i]);
            printf("%s\n", user2.data[iter]);
            strcpy(user2.tipenya[iter], user.tipenya[i]);
            printf("%s\n", user2.data[iter++]);
        }
        dataX++,user2.nKolom = user.nKolom;
        if(!found) fwrite(&user2,sizeof(user2),1,fp1);
	}
	fclose(fp),fclose(fp1),remove(tabel),rename("sementara", tabel);
	return 0;
}

void writelog(char *cmd,char *nama)
{
	time_t rawtime;
	struct tm *timeinfo;
	time(&rawtime);
	timeinfo = localtime(&rawtime);
	char infoWriteLog[1000];
	FILE *file=fopen("logPengguna.log", "ab");
	sprintf(infoWriteLog, "%d-%.2d-%.2d %.2d:%.2d:%.2d::%s::%s\n",timeinfo->tm_year + 1900, timeinfo->tm_mon + 1, timeinfo->tm_mday, timeinfo->tm_hour, timeinfo->tm_min, timeinfo->tm_sec, nama, cmd);
	fputs(infoWriteLog, file),fclose(file);
	return;
}

int main()
{
	int sockfd,ret,newSocket;
    struct sockaddr_in serverAddr;
	struct sockaddr_in newAddr;
	socklen_t addr_size;
	char buffer[1024];
	pid_t childpid;
	sockfd = socket(AF_INET, SOCK_STREAM, 0);
	if(sockfd < 0) printf("[-]Eror pada koneksi.\n"),exit(1);
	printf("[+]Server Socket berhasil dibuat.\n");
	memset(&serverAddr, '\0', sizeof(serverAddr));
	serverAddr.sin_family = AF_INET;
	serverAddr.sin_port = htons(PORT);
	serverAddr.sin_addr.s_addr = inet_addr("127.0.0.1");
	ret = bind(sockfd, (struct sockaddr*)&serverAddr, sizeof(serverAddr));
	if(ret<0) printf("[-]Eror saat binding.\n"),exit(1);
	printf("[+]Nge-bind ke port %d\n", 4444);
	if(listen(sockfd, 10) == 0) printf("[+]Membaca data dari Klien....\n");
	else printf("[-]Eror saat binding.\n");
	while(1)
	{
		newSocket=accept(sockfd,(struct sockaddr*)&newAddr,&addr_size);
		if(newSocket<0) exit(1);
		printf("Connection accepted from %s:%d\n", inet_ntoa(newAddr.sin_addr), ntohs(newAddr.sin_port));
		if((childpid = fork())==0)
		{
			close(sockfd);
			while(1)
            {
				recv(newSocket, buffer, 1024, 0);
				char *token,buffercopy[32000],cmd[100][1005],database_used[1000];
				int i=0;
				strcpy(buffercopy, buffer);
				token = strtok(buffercopy, ":");
				while(token!=NULL)strcpy(cmd[i++], token),token = strtok(NULL, ":");
				if(strcmp(cmd[0], "cUser")==0)
				{
					if(strcmp(cmd[3], "0")==0) bikinAkun(cmd[1], cmd[2]);
					else send(newSocket, "Tidak diizinkan", strlen("Tidak diizinkan"), 0),
						bzero(buffer, sizeof(buffer));
				}
                else if(strcmp(cmd[0], "gPermission")==0)
                {
					if(strcmp(cmd[3], "0")==0)
					{
						if(cekUser(cmd[2]) == 1) beriIzin(cmd[2], cmd[1]);
						else send(newSocket, "Pengguna tidak ditemukan", strlen("Pengguna tidak ditemukan"), 0),
							bzero(buffer, sizeof(buffer));
					}
                    else send(newSocket, "Tidak diizinkan", strlen("Tidak diizinkan"), 0),
						bzero(buffer, sizeof(buffer));
				}
				else if(strcmp(cmd[0], "cDatabase")==0)
                {
					char lokasi[20000];
					snprintf(lokasi, sizeof lokasi, "databases/%s", cmd[1]);
					printf("lokasi = %s, nama = %s , database = %s\n", lokasi, cmd[2], cmd[1]);
					mkdir(lokasi,0777);
					beriIzin(cmd[2], cmd[1]);
				}
				else if(strcmp(cmd[0], "uDatabase") == 0)
                {
					if(strcmp(cmd[3], "0") != 0)
					{
						int permitted = cekAccDB(cmd[2], cmd[1]);
						if(permitted != 1)
							send(newSocket, "Access_database: Tidak diizinkan", strlen("Access_database: Tidak diizinkan"), 0),
							bzero(buffer, sizeof(buffer));
                        else
							strncpy(database_used, cmd[1], sizeof(cmd[1])),
							printf("database_used = %s\n", database_used),
							send(newSocket, "Access_database: Diizinkan", strlen("Access_database: Diizinkan"), 0),
							bzero(buffer, sizeof(buffer));
					}
				}
				else if(strcmp(cmd[0], "cekCurrentDatabase")==0)
                {
					if(database_used[0]=='\0')strcpy(database_used,"Belum memilih database");
					send(newSocket,database_used,strlen(database_used), 0),
					bzero(buffer, sizeof(buffer));
				}
				else if(strcmp(cmd[0], "ctabel")==0)
                {
					printf("%s\n", cmd[1]);
					char *tkn;
					if(database_used[0] == '\0')
						strcpy(database_used, "Belum memilih database"),
						send(newSocket, database_used, strlen(database_used), 0),
						bzero(buffer, sizeof(buffer));
					else
					{
                        char listKueri[100][1005],copycmd[20000],buattabel[20000];
                        snprintf(copycmd, sizeof copycmd, "%s", cmd[1]);
                        tkn=strtok(copycmd, "(), ");
                        int numIter=0,iter=0,iterData=3;;
                        while(tkn!=NULL)
                            strcpy(listKueri[numIter], tkn),
                            printf("%s\n", listKueri[numIter++]),
                            tkn = strtok(NULL, "(), ");
                        snprintf(buattabel, sizeof buattabel, "../database/databases/%s/%s", database_used, listKueri[2]);
                        struct tabel kolom;
                        while(numIter>3)
                            strcpy(kolom.data[iter], listKueri[iterData]),
                            printf("%s\n", kolom.data[iter]),
                            strcpy(kolom.tipenya[iter++], listKueri[iterData+1]),
                            iterData = iterData+2,
                            numIter=numIter-2;
                        kolom.nKolom = iter;
                        printf("iter = %d\n", iter);
                        FILE *fp=fopen(buattabel,"ab");
                        printf("%s\n", buattabel);
                        fwrite(&kolom,sizeof(kolom),1,fp),fclose(fp);
                    }
				}
				else if(strcmp(cmd[0], "dDatabase")==0)
                {
					if(cekAccDB(cmd[2], cmd[1]) != 1)
                    {
						send(newSocket, "Access_database: Tidak diizinkan", strlen("Access_database: Tidak diizinkan"), 0);
						bzero(buffer, sizeof(buffer));
						continue;
					}
                    else
                    {
						char hapus[20000];
						snprintf(hapus, sizeof hapus, "rm -r databases/%s", cmd[1]);
						system(hapus);
						send(newSocket,"Database sudah dihapus",strlen("Database sudah dihapus"), 0);
						bzero(buffer, sizeof(buffer));
					}
				}
				else if(strcmp(cmd[0], "dtabel")==0)
                {
					if(database_used[0] == '\0')
					{
						strcpy(database_used, "Belum memilih database"),
						send(newSocket, database_used, strlen(database_used), 0),
						bzero(buffer, sizeof(buffer));
						continue;
					}
					char hapus[20000];
					snprintf(hapus, sizeof hapus, "databases/%s/%s", database_used ,cmd[1]),remove(hapus);
					send(newSocket, "Tabel berhasil dihapus", strlen("Tabel berhasil dihapus"), 0);
					bzero(buffer, sizeof(buffer));
				}
				else if(strcmp(cmd[0], "dColumn")==0)
                {
					if(database_used[0] == '\0')
					{
						strcpy(database_used, "Belum memilih database"),
						send(newSocket, database_used, strlen(database_used), 0),bzero(buffer, sizeof(buffer));
						continue;
					}
					char buattabel[20000];
					snprintf(buattabel, sizeof buattabel, "databases/%s/%s", database_used, cmd[2]);
					int id=cariKolom(buattabel,cmd[1]);
                    if(id==-1)
                    {
                        send(newSocket,"Kolom tidak ditemukan",strlen("Kolom tidak ditemukan"),0),bzero(buffer, sizeof(buffer));
                        continue;
                    }
					hapusKolom(buattabel, id);
					send(newSocket,"Column Has Been Removed",strlen("Column Has Been Removed"),0),bzero(buffer, sizeof(buffer));
				}
				else if(strcmp(cmd[0], "insert")==0)
                {
                    if(database_used[0] == '\0')
                    {
						strcpy(database_used, "Belum memilih database"),
						send(newSocket, database_used, strlen(database_used), 0),bzero(buffer, sizeof(buffer));
						continue;
					}
                    char listKueri[100][1005],copycmd[20000],*tkn;
					snprintf(copycmd, sizeof copycmd, "%s", cmd[1]);
                    tkn = strtok(copycmd, "\'(), ");
					int numIter=0,banyakKolom;
					while(tkn!=NULL) strcpy(listKueri[numIter++], tkn), tkn = strtok(NULL, "\'(), ");
                    char buattabel[20000];
					snprintf(buattabel, sizeof buattabel, "databases/%s/%s", database_used, listKueri[2]);
                    FILE *fp=fopen(buattabel,"r");
                    if(fp==NULL)
                    {
                        send(newSocket,"Tabel tidak ditemukan",strlen("Tabel tidak ditemukan"), 0),
                        bzero(buffer, sizeof(buffer));
                        continue;
                    }
                    else
                    {
                        struct tabel user;
                        fread(&user,sizeof(user),1,fp),banyakKolom=user.nKolom,fclose(fp);
                    }
					int iter=0,iterData=3;
					struct tabel kolom;
					while(numIter-- > 3)
						strcpy(kolom.data[iter], listKueri[iterData++]),
                        printf("%s\n", kolom.data[iter]),
						strcpy(kolom.tipenya[iter++],"string");
					kolom.nKolom = iter;
                    if(banyakKolom != kolom.nKolom)
                    {
                        send(newSocket, "Input tidak sesuai dengan kolom", strlen("Input tidak sesuai dengan kolom"), 0);
                        bzero(buffer, sizeof(buffer));
                        continue;
                    }
					printf("iter = %d\n", iter);
                    FILE *fp1=fopen(buattabel,"ab");
                    printf("%s\n", buattabel),fwrite(&kolom,sizeof(kolom),1,fp1),fclose(fp1);
					send(newSocket, "Data berhasil dimasukkan", strlen("Data berhasil dimasukkan"), 0);
					bzero(buffer, sizeof(buffer));
                }
                else if(strcmp(cmd[0], "update")==0)
                {
                    if(database_used[0]=='\0')
                    {
						strcpy(database_used, "Belum memilih database"),
						send(newSocket, database_used, strlen(database_used), 0),
						bzero(buffer, sizeof(buffer));
						continue;
					}
                    char listKueri[100][1005],copycmd[20000],*tkn,buattabel[20000];
					snprintf(copycmd, sizeof copycmd, "%s", cmd[1]);
					tkn=strtok(copycmd, "\'(),= ");
					int numIter=0;
					while(tkn!=NULL) strcpy(listKueri[numIter], tkn),
						printf("%s\n", listKueri[numIter++]),
						tkn = strtok(NULL, "\'(),= ");
                    printf("numIter = %d\n", numIter);
					snprintf(buattabel, sizeof buattabel, "databases/%s/%s", database_used, listKueri[1]);
                    if(numIter==5)
                    {
                        printf("buat tabel = %s, kolom = %s", buattabel,listKueri[3]);
                        int id=cariKolom(buattabel, listKueri[3]);
                        if(id==-1)
                        {
                            send(newSocket, "Kolom tidak ditemukan", strlen("Kolom tidak ditemukan"), 0),
                            bzero(buffer, sizeof(buffer));
                            continue;
                        }
                        printf("id = %d\n",id),updateKolom(buattabel,id,listKueri[4]);
                    }
                    else if(numIter==8)
                    {
                        printf("buat tabel = %s, kolom = %s", buattabel,listKueri[3]);
                        int id=cariKolom(buattabel,listKueri[3]);
                        if(id == -1)
                        {
                            send(newSocket,"Kolom tidak ditemukan",strlen("Kolom tidak ditemukan"), 0),
                            bzero(buffer, sizeof(buffer));
                            continue;
                        }
                        printf("%s\n", listKueri[7]);
                        updateKolomWhere(buattabel, id, listKueri[4], cariKolom(buattabel, listKueri[6]),listKueri[7]);
                    }
                    else
                    {
                        send(newSocket,"Data berhasil dihapus",strlen("Data berhasil dihapus"),0),
                        bzero(buffer, sizeof(buffer));
                        continue;
                    }
					send(newSocket,"Data berhasil diupdate",strlen("Data berhasil diupdate"),0);
					bzero(buffer, sizeof(buffer));

                }
                else if(strcmp(cmd[0], "delete")==0)
                {
                    if(database_used[0] == '\0')
                    {
						strcpy(database_used, "Belum memilih database"),
						send(newSocket, database_used, strlen(database_used), 0),
						bzero(buffer, sizeof(buffer));
						continue;
					}
                    char listKueri[100][1005],copycmd[20000],*tkn=strtok(copycmd, "\'(),= "),buattabel[20000];
					snprintf(copycmd, sizeof copycmd, "%s", cmd[1]);
					tkn=strtok(copycmd, "\'(),= ");
					int numIter=0;
					while(tkn!=NULL)
						strcpy(listKueri[numIter],tkn),
						printf("%s\n", listKueri[numIter++]),
						tkn=strtok(NULL, "\'(),= ");
                    printf("numIter = %d\n", numIter);
					snprintf(buattabel, sizeof buattabel, "databases/%s/%s", database_used, listKueri[2]);
                    if(numIter==3) hapusTabel(buattabel, listKueri[2]);
                    else if(numIter==6)
                    {
                        int id = cariKolom(buattabel, listKueri[4]);
                        if(id == -1)
                        {
                            send(newSocket,"Kolom tidak ditemukan",strlen("Kolom tidak ditemukan"),0),
                            bzero(buffer, sizeof(buffer));
                            continue;
                        }
                        printf("id  = %d\n", id);
                        hapusTabelWhere(buattabel, id, listKueri[4], listKueri[5]);
                    }
                    else
                    {
                        send(newSocket,"Input Salah",strlen("Input Salah"),0);
                        bzero(buffer, sizeof(buffer));
                        continue;
                    }
					send(newSocket,"Data berhasil dihapus",strlen("Data berhasil dihapus"),0);
					bzero(buffer, sizeof(buffer));
                }
                else if(strcmp(cmd[0], "select")==0)
                {
                    if(database_used[0]=='\0')
                    {
						strcpy(database_used,"Belum memilih database"),
						send(newSocket, database_used, strlen(database_used), 0),
						bzero(buffer, sizeof(buffer));
						continue;
					}
                    char listKueri[100][1005],copycmd[20000],*tkn=strtok(copycmd, "\'(),= ");
					snprintf(copycmd, sizeof copycmd, "%s", cmd[1]);
					tkn=strtok(copycmd, "\'(),= ");
					int numIter=0;
					while(tkn!=NULL) strcpy(listKueri[numIter], tkn),
						printf("%s\n", listKueri[numIter++]), tkn = strtok(NULL, "\'(),= ");
					printf("ABC\n");
                    if(numIter == 4)
                    {
						char buattabel[20000],cmdKolom[1000];
						snprintf(buattabel, sizeof buattabel, "databases/%s/%s", database_used, listKueri[3]);
						printf("buat tabel = %s", buattabel);
                        printf("masuk 4\n");
                        if(strcmp(listKueri[1], "*")==0)
                        {
                            FILE *fp=fopen(buattabel,"rb"), *fp1;
                            struct tabel user;
                            int id,found=0;
                            char buffers[40000],sendDatabase[40000];
                            bzero(buffer, sizeof(buffer));
                            bzero(sendDatabase, sizeof(sendDatabase));
                            while(1)
                            {
                                fread(&user,sizeof(user),1,fp),snprintf(buffers, sizeof buffers, "\n");
                                if(feof(fp))break;
                                for(int i=0;i<user.nKolom;i++)
                                {
                                    char pd[20000];
                                    snprintf(pd, sizeof pd, "%s\t",user.data[i]);
                                    strcat(buffers, pd);
                                }
                                strcat(sendDatabase, buffers);
                            }
                            send(newSocket, sendDatabase, strlen(sendDatabase), 0);
                            bzero(sendDatabase, sizeof(sendDatabase)),bzero(buffer, sizeof(buffer)),fclose(fp);
                        }
                        else
                        {
                            FILE *fp, *fp1;
                            struct tabel user;
                            int id,found=0;
                            fp=fopen(buattabel,"rb");
                            char buffers[40000];
                            char sendDatabase[40000];
                            bzero(buffer, sizeof(buffer));
                            bzero(sendDatabase, sizeof(sendDatabase));
                            while(1)
                            {
                                fread(&user,sizeof(user),1,fp);
                                snprintf(buffers, sizeof buffers, "\n");
                                if(feof(fp))break;
                                for(int i=0;i<user.nKolom;i++)
                                    if(i==cariKolom(buattabel, listKueri[1]))
                                    {
                                        char pd[20000];
                                        snprintf(pd, sizeof pd, "%s\t",user.data[i]);
                                        strcat(buffers, pd);
                                    }
                                strcat(sendDatabase, buffers);
                            }
                            printf("ini send fix\n%s\n",sendDatabase);
                            fclose(fp),send(newSocket, sendDatabase, strlen(sendDatabase), 0);
                            bzero(sendDatabase, sizeof(sendDatabase)),bzero(buffer, sizeof(buffer));
                        }
                    }
                    else if(numIter==7&&strcmp(listKueri[4],"WHERE")==0)
                    {
						char buattabel[20000];
						snprintf(buattabel, sizeof buattabel, "databases/%s/%s", database_used, listKueri[3]);
						printf("buat tabel = %s", buattabel);
                        char cmdKolom[1000];
                        printf("masuk 4\n");
                        if(strcmp(listKueri[1], "*")==0)
                        {
                            FILE *fp=fopen(buattabel,"rb");
                            struct tabel user;
                            int id,found=0;
                            char buffers[40000],sendDatabase[40000];
                            bzero(buffer, sizeof(buffer));
                            bzero(sendDatabase, sizeof(sendDatabase));
                            while(1)
                            {
                                char enter[] = "\n";
                                fread(&user,sizeof(user),1,fp);
                                snprintf(buffers, sizeof buffers, "\n");
                                if(feof(fp)){
                                    break;
                                }
                                for(int i=0; i< user.nKolom; i++)
                                    if(strcmp(user.data[cariKolom(buattabel, listKueri[5])], listKueri[6])==0)
                                    {
                                        char pd[20000];
                                        snprintf(pd, sizeof pd, "%s\t",user.data[i]);
                                        strcat(buffers, pd);
                                    }
                                strcat(sendDatabase, buffers);
                            }
                            send(newSocket, sendDatabase, strlen(sendDatabase), 0);
                            bzero(sendDatabase, sizeof(sendDatabase));
                            bzero(buffer, sizeof(buffer)),fclose(fp);
                        }
                        else
                        {
                            FILE *fp, *fp1;
                            struct tabel user;
                            int id,found=0;
                            int idUbah = cariKolom(buattabel, listKueri[5]);
                            fp=fopen(buattabel,"rb");
                            char buffers[40000];
                            char sendDatabase[40000];
                            bzero(buffer, sizeof(buffer));
                            bzero(sendDatabase, sizeof(sendDatabase));
                            while(1)
                            {
                                char enter[] = "\n";
                                fread(&user,sizeof(user),1,fp);
                                snprintf(buffers, sizeof buffers, "\n");
                                if(feof(fp))break;
                                for(int i=0;i<user.nKolom;i++)
                                    if(i==cariKolom(buattabel, listKueri[1])&&
                                       (strcmp(user.data[idUbah], listKueri[6])==0
                                        ||strcmp(user.data[i],listKueri[5])==0))
                                    {
                                        char pd[20000];
                                        snprintf(pd, sizeof pd, "%s\t",user.data[i]),
                                        strcat(buffers,pd);
                                    }
                                strcat(sendDatabase, buffers);
                            }
                            printf("ini send fix\n%s\n", sendDatabase),fclose(fp);
                            send(newSocket, sendDatabase, strlen(sendDatabase), 0);
                            bzero(sendDatabase, sizeof(sendDatabase)),bzero(buffer, sizeof(buffer));
                        }
                    }
                    else
                    {
						printf("ini query 3 %s", listKueri[numIter-3]);
						if(strcmp(listKueri[numIter-3], "WHERE")!= 0)
						{
							char buattabel[20000];
							snprintf(buattabel, sizeof buattabel, "databases/%s/%s", database_used, listKueri[numIter-1]);
							printf("buat tabel = %s", buattabel);
							printf("tanpa where");
							int id[100],j=0;
							for(int i=1;i<numIter-2;i++)
								id[j]=cariKolom(buattabel, listKueri[i]),
								printf("%d\n", id[j++]);
						}
                        else if(strcmp(listKueri[numIter-3], "WHERE")== 0) printf("dengan where");
					}
                }
                else if(strcmp(cmd[0], "log")==0) writelog(cmd[1], cmd[2]),
					send(newSocket,"\n",strlen("\n"),0),bzero(buffer, sizeof(buffer));
				if(strcmp(buffer, ":exit") == 0)
				{
					printf("Terdiskoneksi dari %s:%d\n", inet_ntoa(newAddr.sin_addr), ntohs(newAddr.sin_port));
					break;
				}
				else printf("Klien: %s\n", buffer),send(newSocket,buffer,strlen(buffer),0),bzero(buffer,sizeof(buffer));
			}
		}
	}
	close(newSocket);
	return 0;
}
